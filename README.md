# GIT

## About

This repo is design for student that want to understand how to use git on Unix environment. It was created by Fabian for ESME Sudria student.

## Content

This coures has tree steps that correspond to folder 

- get-started-with-git-localy 
- get-started-with-git-remote-repository 
- get-started-with-git-lab

In each of this folder, you will find all informations to understand the pupose of the tutoriel in a README.md file. You will also find some exercice to do by your self.
