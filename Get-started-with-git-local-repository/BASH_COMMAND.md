# Bash Command

## About

this document attempt to give a list of basic bash command that you need to know to use shell (terminal). It was created for student that never use bash terminal before.

## List of command 

In the title of each command you will find Capital letter, those letter join together make te command
if your not use how to run this command, it's allways a good idea to ask for HELP. To do so run 

'<COMMAND> --help' example `cd --help`

```
If you need more information you also can ask for MANuel of the command

`man <COMMAND>` example `mand cd`

### Navigation inside filesystem

#### Change Directory
`cd` example `cd ..`
**TIPS**:`.` means this directory

#### LiSt
`ls <DIRECTORY_WHERE_YOU_WANT_INFO>` example `ls -lairth ~`
Good to know:
~ the tild means the value of HOME variable, if you don't know it use `echo` [command](#echo)

#### Pass Working Directory
`pwd` example `pwd .`

### Creat, delet and edit files and directoris

#### MaKe DiRectroy
`mkdir <NEW_DIRECTORY>` example `mkdir -p GitCours/TW/FirstExercice`
**TIPS**: -p allow you to create directory inside directory

#### TOUCH
`touch <MY_NEW_FILE>`touch test.c`
this command create a file

#### ReMove
`rm <DIRECTORY_OR_FILE>` exampel `rm -rf GitCours`
**TIPS**: -rf allow you to force 

#### MoVe
`mv <OLD_FILE_OR_DIRECTORY_NAME> <NEW_FILE_OR_DIR_NAME>

#### WHICH
`which`

#### ENVironement
`env`
give you information about variables set inside your environement

### ECHO
`echo` example: `echo $EDITOR`
**TIPS**: $VARIBLE

###
``

## List of usefull utility
``

###
``

###
``

### MANuel
`man`
