# Get Started With Git

## About

This document explain how to start working with git in local Unix environement using git command in bash shell terminal. It's design for begginers and will focus on Git command.

## Table of contente

1.[Installation](#installation)
1.1[OS](#os)
1.1.1[Linux](#linux)
1.1.1[Windows](#windows)
1.1.1[MAC](#windows)
1.2.[Git](#git)
2.[Git](#git-1)
2.1[Initialisation](#initialisation)
2.2[Environement](#environement)
2.2[Developement](#developement)
## Installation
### OS
#### Windows

If you have a windows OS you have to install Windows Sub Linux (WSL), thanks to wsl you can install any linux distributions
on you computor. To install it, go into Microsoft Store and look for linux distribution that you want to install (choose Ubuntu if you don't know). This is not mandatory but you should also install Windows Terminal from the same store.
Open a `Powershell` as adminitrator and tape this commande `dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
Restart you're computor.
You can now open Windows Terminal, create a new tab with the linux distribution that you want to use. [continu to follow instllation instruction](#git)

#### Linux

Any linux distribution is a Unix base OS, open a terminal ( on ubuntu `ctrl + shift + t`) and [continu to follow instllation instruction](#git)

#### Mac

Mac is a Unix base OS, press `command + space` and find terminal to follow this tutorial. You also can install iterm2 that make terminal a lit bit better that the default one. when you terminal is open your ready to install git 

### Git
When you have a termnal ready to use run the command start by running `git` command in your terminal. If you have the outuput is :

```
USER@MACHINE:~$ git
fatal: not a git repository (or any of the parent directories): .git
```

You can go to the [beggining of this tutorial](#repo-initialisation)

If your got this output :

```
USER@MACHINE:~$ git
-bash: git: command not found
```
This mean that git is not install on your distribution.
install the package name git to do so, launch this command in the terminal :

```
USER@MACHINE:~$ sudo apt update && sudo apt install -y git
```
try again the `git` command, it should tell you that your not in a git repository as shown above.

## Git

### Initialisation

To work with git we need to convert a regular folder to a git repository, follow thoses steps :

Go inside a folder where you want to creat your new project:
```
USER@MACHINE:~$ cd $PASS_TO_YOU_DIRECTORY
```
example: `cd /mnt/c/Users/fabia/Documents/Git_cours`
**Tips**: if you are new to bash shell and you need help with basics command (ls, pwd, mkdir, cat ...), you can open BASH_COMMAND.md to have a list of command with explanations.

Create and go into a new directory that will host your new project
```
USER@MACHINE:~/PATH_TO_YOUR_DIRECTORY/$ mkdir $My_awesome_project && cd $My_awesome_project
```
example: `mkdir 1-exercice && cd 1-exercice`
**Tips**: you can run bash command one afeter an other wither symbole && the second command will run only if the first one return without any errors.

Be sure that everything when well and that you are in your new directory
Command `pwd` should return `/home/$User/<PATH_TO_YOUR_DIR>` example: `pwd`-> `/mnt/c/Users/fabia/Documents/Git_cours`

We can initialize this directory as a git repository

```
USER@MACHINE:~/PATH_TO_YOUR_DIRECTORY/1-exercice$ git init
Initialized empty Git repository in /home/USER/PATH_TO_YOUR_DIRECTORY/1-exercice/.git/
```
As write above, git created an hidden folder call .git with data inside making this folder a git repository.

When your are in a git repository, you can seet the state of this repo with :

```
USER@MACHINE:~/PATH_TO_YOUR_DIRECTORY/1-exercice$ git status
on branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)

```
Git tell us that we are on branch master, that we did not yet commit any thing and that currently there is nothing to commit.

So we can start the devellopement of our application

choose your favoris C Intergrated Devellopement Platform (IDE) to start your this project. If you don't know which one choose use [Visual studio code](https://code.visualstudio.com/download)
**Good to know**: visual code want help you writting code if you don't add plugging for the language you want to do. The first time you arrive on a new document with extention .c, visual should ask you if you want to install plugin for **C**. You should say YES !
**TIPS**: if you aim to be a professional devellopers and want to write code fast, vim or neovim should be your choce. It's really hard to pick-up because you have to learn all the shortcut to use it be it makes devellopement a lot faster when you know how to use it.

create a new file main.c

```
<USER_NAME>@<MACHINE_NAME>: touch main.c
```

again, check your git repository status

```bash
<USER_NAME>@<MACHINE_NAME>: git status
```
```bash
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        main.c

nothing added to commit but untracked files present (use "git add" to track)

```
git tell you almost the same things as the first time but it also list files that are not track yet. Here the file that is not track is main.c
start tracking this file
```bash
<USER_NAME>@<MACHINE_NAME>: git add main.c
```

ask for repo status:

```bash
<USER_NAME>@<MACHINE_NAME>: git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
        new file:   main.c

```

git tells you that you are ready to commit changes since last time. But we have just created a file and that is "not enought" to track change. so let edit this file to create a hello world example

To do so Start your IDE and open your directory

```
$<USER_NAME>@<MACHINE_NAME>$ code .
vim main.c
nano main.c
```

write a code to print hello world inside this file

```c
#include <stdio.h>

int main(){

    printf("hello world\r\n");
    return 0;
}
```

compile your code and test it

```
<USER_NAME>@<MACHINE_NAME>$ gcc main.c -o hello
./hello
```

this should print `hello world` on your screen.

Now that we have code that work, we are ready to save our work inside our fist "version" thanks to **git**
So we will add all the element we wrote since last action and add a commit on the currant branch (master) to commit all we have done.

```bash
git commit -m "Init(hello world): print hello world when you run the executable"
```

now if you check your repo status 

```bash
git status
```
As we can see, git tell use that one file is untrack: hello executable. But the truth is that we only want to push source code and note the executable. So we are going to add tell git to ignore the executable.

```bash
<USER_NAME>@<MACHINE_NAME>$ echo "hello" >> .gitignore
```
repo status:
```bash
<USER_NAME>@<MACHINE_NAME>$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        .gitignore

nothing added to commit but untracked files present (use "git add" to track)
```

we can see that there hello executable is not untrack anymore but ignore ! 
But no the .ignore file is untrack. Add it to your repo and commit the work to save it

### Branchs

Since the begging, we have work on the same branch master. This is a bad idea, we should allways create feature branch to add feature. Like this, we **always** have a source code without bugs on master!

As we don't want to use gcc command each time we have made changes, we will create a makefile to compile our code with make

```bash
<USER_NAME>@<MACHINE_NAME>$ git branch feat-makefile
```

```bash
<USER_NAME>@<MACHINE_NAME>$ git branch
```

you can see that there is a new branch, so you can go on it before to make change.

```bash
<USER_NAME>@<MACHINE_NAME>$ git checkout feat-makefile
```

repo status

```bash
git status
```

add a makefile

```bash
touch Makefile
```

```bash
all: hello

hello: main.c
	gcc main.c -o hello
```

### Tags

when you want to publish your code, you should always give a version. You can see that almost all executable that use implement flag --version to know which version you have.
version are design like this X.Y.Z
where X is **Major release**
where Y is **feature info**
and Z is **bug fix**
