# Git-lab

## About

This document explain how to use git lab efficiently.

## Table of contents

1.[Groups](groups)
2.[Project](project)
2.1[Set-up](set-up)
2.2[Branch & Environment](branch-&-environment)
3.[Issues](issues)
4.[Milstone](milstone)

## Get started 

Gitlab is an other web service that offer git repository space as Github, execpt that Gitlab is open-source. It means that if you want you can collaborate and add feature to gitlab, fork it to creat your own version ... 

[Creat a gitlab](https://gitlab.com/users/sign_up) account and sign in it to gitlab.

## Groups 

When you arrive for the first time, they will ask you to creat a Group. Group are like folders in you computor, thanks to them you can gather projects. For example when I'm working on a project, I usually create a group and subgroup like this :
* Name of my project
	* Hardware 
	* Firmware
	* Back-end
	* Front-end

In Hardware subGroup, you will find :
- a project with all source code of PCB for example.
- Tools that can be helpfool to 

In you case, you can create a Group name with you group classmate name and create a subgroup name Git to push all projects that we are gooing to make together.

## Project

### Set-up
When you creat a new project, there is two possibility: 
1. you are working on a current project and you already have source code, then create a blanc project and **do not initialize it with README file**
Then copy your repo url and add a remote to you're local repository containing all source code as we so in step *Get-started-with-remote-repository*. For example if your are working on MicroControl Unit (MCU) source code (firmware) you will probably start from a example created by the IDE and use this method.

2. you will write code from scratch and do everything on your own (as we did on exercice-1 of *Get-started-with-remote-repository*) then you can creat a blanc project but **initize it with README file**

### Branch & Environment 
As we so in *Get-started-with-git-local-repository*, we need to creat branchs that match with environement. First branch will be master, on this branch you will find the source code that run on production. The second branch will be the branch develop and will contain source code with all new feature that run on staging env. To do so you don't have to do it localy as we did in get-started-with-git-local-repostiroy, you can create a new branch from gitlab.com GUI. Go to [gitlab.com](gitlab.com), on the left menu go to `Repository`->`Branches` and click on green bouton **New Branch** and creat the branch for staging env (develop).

As you don't want to push code on branch that correspond to production environement and always start from the one that correspond to staging environement, you have to change repository setting. By default the branch master is the default branch, change this and set develop as default branch :

On the left menu go to `Settings` -> `Repository`and expand `Default Branch` and change 

## Issues

When we start a new project, we often have a plan to get it done. It correspond to a list of task to do. Example :

Project: I want to go ski this winter.

Plan:
 * find frends that want to join
 * find the ski resort that will make every-one happy 
 * rent an appartment there
 * buy train tickes 
 * and so on 

As any project, when we start developping an application you need to plan your work and define tasks :

Project: Create an connected object to see room occupation during the day

plan:
 * Write users story to define what the product do
 * From user story, write techincal specification and test that will validate specification
 * Choose sensors
 * make circuit board
 * create a git-lab repository for firmware
 * make led blink, to make sure that everything is set correctly (electronics hello world)
 * make communication with sensor 1
 * initialize bluetooth communication
 * send information over bluetooth   
 * ....

Git-lab allow you to plan you work and creat issues associate with a poject for all the task that you need to do. If we take example above, we could create à project with firmware source code that contain last 5 issues. That way, you have a dashboard where you can see all the work to do. You also can see who is working on a specific task. This is a creat tool to manage you project. 

To create issue :

On the left menu got to `issues` and click on **New issue**

when creating a issue, it important to identify what it's all about. To do so, I usualy name is like this :

`feat(NAME_OF_MY_FEATURE):DESCRITION_OF_MY_FITURE`

or

`fix(BUG_FIX_NAME):DESCRITION_OF_BUG_TO_RESOLVE`

You also can add tags with specific.

## Milstones

When you have cut your work in issues, you can also plan when you will doing those issues. This allow you to specify a goal for the next periode of time that you define and be sure that you will not forget any thing to do. 
